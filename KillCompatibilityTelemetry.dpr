﻿program KillCompatibilityTelemetry;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  System.SysUtils,
  Shellapi,
  Tlhelp32,
  Windows;

var
  Result: Boolean = False;
  count: Integer = 0;

const
  fileName: String = 'CompatTelRunner.exe';

function processExists(exeFileName: string): Boolean;
var
  ContinueLoop: Boolean;
  FSnapshotHandle: THandle;
  FProcessEntry32: TProcessEntry32;

begin
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
  ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);
  Result := False;

  while Integer(ContinueLoop) <> 0 do
  begin

    if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile))
      = UpperCase(exeFileName)) or (UpperCase(FProcessEntry32.szExeFile)
      = UpperCase(exeFileName))) then
    begin
      Result := True;
      WriteLn('Microsoft Compatibility Telemetry has been found');
    end;

    ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
  end;

  CloseHandle(FSnapshotHandle);
end;

function KillTask(exeFileName: string): Integer;
const
  PROCESS_TERMINATE = $0001;

var
  ContinueLoop: BOOL;
  FSnapshotHandle: THandle;
  FProcessEntry32: TProcessEntry32;

begin
  Result := 0;
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
  ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);

  while Integer(ContinueLoop) <> 0 do
  begin

    if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile))
      = UpperCase(exeFileName)) or (UpperCase(FProcessEntry32.szExeFile)
      = UpperCase(exeFileName))) then
    begin
      Result := Integer(TerminateProcess(OpenProcess(PROCESS_TERMINATE, BOOL(0),
        FProcessEntry32.th32ProcessID), 0));
      WriteLn('Microsoft Compatibility Telemetry has been killed');
    end;

    ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
  end;

  CloseHandle(FSnapshotHandle);
end;

procedure ClearConsoleScreen;
const
  BUFSIZE = 80 * 25;

var
  Han, Dummy: LongWord;
  buf: string;
  coord: TCoord;

begin
  Han := GetStdHandle(STD_OUTPUT_HANDLE);

  if Han <> INVALID_HANDLE_VALUE then
  begin

    if SetConsoleTextAttribute(Han, FOREGROUND_RED or FOREGROUND_GREEN or
      FOREGROUND_BLUE) then
    begin
      SetLength(buf, BUFSIZE);
      FillChar(buf[1], Length(buf), ' ');

      if WriteConsole(Han, PChar(buf), BUFSIZE, Dummy, nil) then
      begin
        coord.X := 0;
        coord.Y := 0;
        if SetConsoleCursorPosition(Han, coord) then
          Result := True;
      end;

    end;

  end;
end;

begin
  Result := False;

  try
    { TODO -oUser -cConsole Main : Insert code here }

    while 0 <> 1 do
    begin
      Sleep(3000);

      try
        Result := processExists(fileName);
      except
        on E: Exception do
        begin
          WriteLn('Error: ' + E.Message);
          WriteLn('Stack Trace: ' + E.StackTrace);
          Result := False;
        end;
      end;

      if Result then
      begin
//        ClearConsoleScreen;

        try
          KillTask(fileName);
          count := count + 1;
          Write('Process terminated! Terminated ' + IntToStr(count));

          if count = 1 then
            WriteLn(' process so far!')
          else
            WriteLn(' processes so far!');

        except
          on E: Exception do
          begin
            WriteLn('Error: ' + E.Message);
            WriteLn('Stack Trace: ' + E.StackTrace);
          end;
        end;

        Result := False;
      end
      else
      begin
        //ClearConsoleScreen;
        Write(TimeToStr(Time) + ' ' + DateToStr(Date));
        WriteLn(' Monitoring in progress...');
      end;
    end;

  except
    on E: Exception do
      WriteLn(E.ClassName, ': ', E.Message);
  end;

end.
